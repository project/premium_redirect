PREMIUM REDIRECT
================
by jason.xie@victheme.com

Premium redirect is a simple module for extending premium content module 
(http://drupal.org/project/premium_content) the purpose of this module is
to provide a redirection to a configurable page (node) when a user without
sufficient access level is trying to access a premium content.

Currently the module is only capable to do a global redirection for all
premium content level.