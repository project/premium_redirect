<?php

/**
 * @file
 *
 * Administration form for premium redirect module.
 */

/**
 * Menu callback function for configuration form
 */
function premium_redirect_settings($form, $form_state) {
  $form['premium_redirect_nid'] = array(
    '#type' => 'textfield',
    '#title' => t('Node for insufficient access'),
    '#description' => t('Enter the node id as redirect target to show when a user without sufficient access right is trying to access a premium content.'),
    '#default_value' => variable_get('premium_redirect_nid', ''),
    '#field_prefix' => 'node/',
    '#size' => '5',
  );

  return system_settings_form($form);
}